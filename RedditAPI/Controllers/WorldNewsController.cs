﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using RedditDAL.Models;
using RedditDAL.Repos;
using AutoMapper;

namespace RedditAPI.Controllers
{
    [RoutePrefix("api/WorldNews")]
    public class InventoryController : ApiController
    {
        public InventoryController()
        {
            //it is now in Global.asax so that it only runs once!
            //Mapper.Initialize(
            //    cfg =>
            //    {
            //        cfg.CreateMap<Inventory, Inventory>()
            //        .ForMember(x => x.Orders, opt => opt.Ignore());
            //    });
        }

        private readonly BaseRepo<WorldNew> _repo = new BaseRepo<WorldNew>();

        //get: api/WorldNews
        [HttpGet, Route("")]
        public IEnumerable<WorldNew> GetNews()
        {
            var worldnews = _repo.GetAll();
            return Mapper.Map<List<WorldNew>, List<WorldNew>>(worldnews);
        }

        //GET api/WorldNews/1
        [HttpGet, Route("{id}", Name = "DisplayRoute")]
        [ResponseType(typeof(WorldNew))]
        public async Task<IHttpActionResult> GetNews(int id)
        {
            WorldNew inventory = _repo.GetOne(id);
            if (inventory == null)
            {
                return NotFound();
            }
            return Ok(Mapper.Map<WorldNew, WorldNew>(inventory));
        }

        [HttpPut, Route("{id}")]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutNews(int id, WorldNew worldNew)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != worldNew.Id)
            {
                return BadRequest();
            }
            try
            {
                _repo.Save(worldNew);
            }
            catch (Exception ex)
            {
                //production app should do more here
                throw;
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        //Post: api/WorldNews
        [HttpPost, Route("")]
        [ResponseType(typeof(WorldNew))]
        public IHttpActionResult PostNews(WorldNew worldNew)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                _repo.Add(worldNew);
            }
            catch (Exception ex)
            {
                //production app should do more here
                throw;
            }
            return CreatedAtRoute("DisplayRoute", new { id = worldNew.Id }, worldNew);
        }

        //DELETE: api/WorldNews/5
        [HttpDelete, Route("{id}")]
        [ResponseType(typeof(void))]
        public IHttpActionResult DeleteNews(int id)
        {
            try
            {
                _repo.Delete(id);
            }
            catch (Exception ex)
            {
                //production app should do more here
                throw;
            }
            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _repo.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}