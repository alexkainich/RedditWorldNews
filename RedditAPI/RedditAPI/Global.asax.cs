﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using RedditDAL.Models;
using AutoMapper;
using RedditDAL.EF;
using System.Data.Entity;

namespace RedditAPI
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            Mapper.Initialize(
               cfg =>
               {
                   cfg.CreateMap<WorldNew, WorldNew>();
               });

            //re-create the database every time the application is run. Is ensures a consisten experience while developing the app
            Database.SetInitializer(new MyDataInitializer());
        }
    }
}
